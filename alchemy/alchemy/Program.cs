﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace alchemy
{
    class Program
    {
        static void Main(string[] args)
        {
            Book book1 = new Book();
            book1.FileReader();

            graf test1 = new graf(book1.attempts.Length);

            for (int i = 0; i < book1.attempts.Length; i++)
            {
                test1.AddPoint(book1.attempts[i].starter, book1.attempts[i].result, (int)book1.attempts[i].catalyser);
            }

            test1.Dijkstra(1, 0);
            Console.ReadLine();
        }
    }
}

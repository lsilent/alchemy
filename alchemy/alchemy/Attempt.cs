﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace alchemy
{
    class Attempt
    {
        public int starter;
        public char catalyser;
        public int result;

        public Attempt(int start, char cat, int result)
        {
            this.starter = start;
            this.catalyser = cat;
            this.result = result;
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace alchemy
{
    class graf
    {
        class GrafPoint
        {
            public int value;
            public int distance;
        }

        int numberofpoints;
        List<GrafPoint>[] neighborlist;
        List<int> container;

        public graf(int numberofpoints)
        {
            this.numberofpoints = numberofpoints;
            neighborlist = new List<GrafPoint>[numberofpoints];
            for (int i = 0; i < neighborlist.Length; i++)
            {
                neighborlist[i] = new List<GrafPoint>();
            }

            container = new List<int>();
        }

        public void AddPoint(int from, int to, int value)
        {
            GrafPoint gp = new GrafPoint();
            gp.value = to;
            gp.distance = value;

            neighborlist[from].Add(gp);
        }

        public List<int> GetNeighbors(int point)
        {
            List<int> neighbors = new List<int>();
            for (int i = 0; i < neighborlist[point].Count; i++)
            {
                GrafPoint gp = neighborlist[point][i];
                neighbors.Add(gp.value);
            }
            return neighbors;
        }

        public int LowestOut(double [] d)
        {
            double min = double.PositiveInfinity;
            int min_index = -1;

            for (int i = 0; i < container.Count; i++)
            {
                if (d[container[i]] < min)
                {
                    min = d[container[i]];
                    min_index = i;
                }
            }

            int needtodelete = container[min_index];
            container.Remove(needtodelete);
            return needtodelete;
        }

        public void Dijkstra(int from, int to)
        {
            int[] n = new int[numberofpoints];
            double[] d = new double[numberofpoints];
            int[] charcode = new int[numberofpoints];
            for (int i = 0; i < d.Length; i++)
            {
                d[i] = double.PositiveInfinity;
                container.Add(i);
            }
            d[from] = 0;
            while (container.Count != 0)
            {
                int u = LowestOut(d);
                List<int> x = GetNeighbors(u);
                for (int i = 0; i < x.Count; i++)
                {
                    double distance = 0;
                    List<GrafPoint> neighbors1 = neighborlist[u];
                    for (int j = 0; j < neighbors1.Count; j++)
                    {
                        if (neighbors1[j].value == x[i])
                        {
                            distance = neighbors1[j].distance;
                        }
                    }


                    if (d[u] + distance < d[x[i]])
                    {
                        d[x[i]] = d[u] + distance;
                        n[x[i]] = u;
                        charcode[x[i]] = (int)distance;
                        
                    }
                }
            }


            int k = 0;
            int osszeg = 0;
            int seged = 0;
            char[] catalys = new char[numberofpoints];
            int s = 0;
            while (osszeg < (int)d[0])
            {
                osszeg = osszeg + charcode[seged];
                //Write("{0} ",(char)charcode[seged]);
                catalys[s] = (char)charcode[seged];
                s++;
                k = (int)n[seged];
                seged = (int)n[seged];
                
            }

            int num = 0;
            for (int i = 0; i < catalys.Length; i++)
            {
                if (catalys[i] != '\0')
                {
                    num++;
                }
            }
            
            string[] catalys4real = new string[num];
            int num1 = 0;
            for (int i = num-1; i >= 0 ; i--)
            {
                catalys4real[i] += catalys[num1];
                num1++;
            }
            string path = @"D:\private\ITsec\practice\alchemy\alchemy\bin\Debug\aranyki.txt";
            System.IO.File.WriteAllLines(path, catalys4real);
            
        }

    }
}
